<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::post('google/webhook', App\Http\Controllers\GoogleWebhookController::class)->name('google.webhook');

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('event', [App\Http\Controllers\EventController::class, 'index'])->name('event.index');

Route::get('google', [App\Http\Controllers\GoogleAccountController::class, 'index'])->name('google.index');
Route::get('google/oauth', [App\Http\Controllers\GoogleAccountController::class, 'store'])->name('google.store');
Route::get('google/callback', [App\Http\Controllers\GoogleAccountController::class, 'store'])->name('google.callback');
Route::delete('google/{googleAccount}', [App\Http\Controllers\GoogleAccountController::class, 'destroy'])->name('google.destroy');
