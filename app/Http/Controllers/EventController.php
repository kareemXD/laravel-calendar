<?php

namespace App\Http\Controllers;

use App\Models\Calendar;
use App\Services\Google;
use Illuminate\Http\Request;
use App\Jobs\SynchronizeGoogleEvents;

class EventController extends Controller
{
    public function index()
    {
        $events = auth()->user()->events()
            ->orderBy('started_at', 'desc')
            ->get();

        return view('events', compact('events'));
    }
}
